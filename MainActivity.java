package com.example.timer;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Chronometer;
import android.widget.TextView;

import com.example.timer.R;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity{

    private static final String CHANNEL_ID = "channel1";

    // chronometer is inbuild counting field which acts as a counter

    private Chronometer chronometer;
    private boolean running;
    String massege;


    public static String mypackage="gattanisanket.com.mytimer";

    public static String hr="hr";
    public static String min="min";
    public static String sec="sec";
    public static String phase="phase";
    KeyguardManager myKM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Notification");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        chronometer = findViewById(R.id.cornometer);



        // to start counting of chronometer
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();
            running = true;
        }


    }


    @Override
    protected void onPause() {
        super.onPause();
        myKM= (KeyguardManager) getBaseContext().getSystemService(Context.KEYGUARD_SERVICE);
        if(! myKM.inKeyguardRestrictedInputMode()) {
            Log.d("msg","locked");
            int endhr,endmin,endsec,endphase;

            // geting current system time
            Calendar calendar=Calendar.getInstance();
            endhr=calendar.get(Calendar.HOUR);
            endmin=calendar.get(Calendar.MINUTE);
            endsec=calendar.get(Calendar.SECOND);
            endphase=calendar.get(Calendar.AM_PM);

            // storing time in shared preferences at the time of locking the app

            SharedPreferences sp=getSharedPreferences(mypackage, Context.MODE_PRIVATE);
            sp.edit().putInt(hr,endhr).commit();
            sp.edit().putInt(min,endmin).commit();
            sp.edit().putInt(sec,endsec).commit();
            sp.edit().putInt(phase,endphase).commit();

        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        myKM= (KeyguardManager) getBaseContext().getSystemService(Context.KEYGUARD_SERVICE);
        if( myKM.inKeyguardRestrictedInputMode()) {
            Log.d("msg","unlocked");
            int currenthr, currentmin, currentsec;
            int currentphase;

            //fetching current time from system

            Calendar calendar = Calendar.getInstance();
            currenthr = calendar.get(Calendar.HOUR);
            currentmin = calendar.get(Calendar.MINUTE);
            currentsec = calendar.get(Calendar.SECOND);
            currentphase = calendar.get(Calendar.AM_PM);

            // fetching  lock time stored in shared preferences at the time of closing the app

            SharedPreferences sp = getSharedPreferences(mypackage, Context.MODE_PRIVATE);

            int prehr = sp.getInt(hr, currenthr);
            int premin = sp.getInt(min, currentmin);
            int presec = sp.getInt(sec, currentsec);
            int prephase = sp.getInt(phase, currentphase);


            // performing calculations for time difference which is to be displayed on notification panel

            if ((currentphase == 0 && prephase == 1) || (currentphase == 1 && prephase == 0)) {
                currenthr = currenthr + 12;
            } else {
                if (presec > currentsec) {
                    currentsec += 60;
                    currentsec = currentsec - presec;
                    currentmin--;
                    if (currentmin < 0) {
                        currentmin = 59;
                        currenthr--;
                    }

                } else {
                    currentsec -= presec;
                }
                if (currentmin < premin) {
                    currentmin += 60;
                    currentmin -= premin;
                    currenthr--;
                } else {
                    currentmin -= premin;
                }
                currenthr -= prehr;


            }


            massege = "" + currenthr + " hr  : " + currentmin+ " min  : " + currentsec + " sec";


            //code for notification(displaying notification)

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("New notification")
                    .setContentText("App was closed for  " + massege)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat notificationManagerCompat;
            notificationManagerCompat = NotificationManagerCompat.from(this);
            notificationManagerCompat.notify(1, builder.build());

            //resetting the chronometer
            running=false;
            //starting chronometer
            if (!running) {
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                running = true;
            }

        }
    }


}



